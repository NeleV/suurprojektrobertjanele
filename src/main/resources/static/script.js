var url = "http://localhost:8080/tk_edu";
var urlReg = "http://localhost:8080/tk_reg";

//var url = location.origin + "/tk_edu"; //Aadress, kus info võtmine ja tagasi saatmine toimub
//var urlReg = location.origin + "/tk_reg";

console.log("Töötame hullumoodi")

document.querySelector('form').onsubmit = async function(event) { //Funktsioon millega kogume info HTML formist kokku ja saadame JSONiga SQLi
    event.preventDefault()

    var kuupaev = document.querySelector('#kuupaev').value //1. loome samanimelised muutujad, millega korjame formist info kokku
    var algusaeg = document.querySelector('#algusaeg').value
    var lopuaeg = document.querySelector('#lopuaeg').value
    var kool = document.querySelector('#kool').value
    var aadress = document.querySelector('#aadress').value
    var linnmk = document.querySelector('#linnmk').value
    var teema = document.querySelector('#teema').value
    var klass = document.querySelector('#klass').value
    var opilastearv = document.querySelector('#opilastearv').value
    var kontaktisik = document.querySelector('#kontaktisik').value
    var kontaktemail = document.querySelector('#kontaktemail').value
    var kontaktnumber = document.querySelector('#kontaktnumber').value
    console.log(kuupaev, algusaeg, lopuaeg, kool, aadress, teema, klass, opilastearv, kontaktisik, kontaktemail, kontaktnumber)

    await fetch(url, {
        method: "POST",
        body: JSON.stringify({
            kuupaev,
            algusaeg,
            lopuaeg,
            kool,
            aadress,
            linnmk,
            teema,
            klass,
            opilastearv,
            kontaktisik,
            kontaktemail,
            kontaktnumber
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
    uuendaSisestust() //KÄIVITAME FUNKTSIOONI!!!
}

var uuendaSisestust = async function() { //6. funktsioon mille sees pärime info SQList tagasi
    console.log("uuendaSisestust l2ks k2ima")

    var request = await fetch(url) //8.fetch teeb päringu serverisse (meie defineeritud aadress)
    var sisestus = await request.json() //9.json() käsk vormib meile data mugavaks JSONiks
    console.log(sisestus)
    //console.log(json)
    var loenguteHTML = "<table style='width:100%'><tr><th>Kuupäev</th><th>Algusaeg</th> <th>Lõpuaeg</th> <th>Kool</th><th>Aadress</th><th>Linn/Maakond</th><th>Teema</th><th>Mitmes klass</th><th>Õpilaste arv</th><th>Registreeru!</th></tr>"

    for (var loeng of sisestus) {
        var id = loeng.id
        var kuupaev = loeng.kuupaev
        var algusaeg = loeng.algusaeg
        var lopuaeg = loeng.lopuaeg
        var kool = loeng.kool
        var aadress = loeng.aadress
        var linnmk = loeng.linnmk
        var teema = loeng.teema
        var klass = loeng.klass
        var opilastearv = loeng.opilastearv

        loenguteHTML += `
            <tr>
                <td>${kuupaev}</td>
                <td>${algusaeg}</td>
                <td>${lopuaeg}</td>
                <td>${kool}</td>
                <td>${aadress}</td>
                <td>${linnmk}</td>
                <td>${teema}</td>
                <td>${klass}</td>
                <td>${opilastearv}</td>
                <td>
                    <form class="registreeri-vorm">
                        <input value="${id}" type = "hidden"/>
                        <input id="registreerija" placeholder="Sisesta email"/>
                        <input id="sisestanupp" type="submit"><br>
                    </form>
                </td>
            </tr>
        `
    }
    loenguteHTML = loenguteHTML + "</table>"
    document.querySelector("#loengukoht").innerHTML = loenguteHTML

    var koikVormid = document.querySelectorAll(".registreeri-vorm")
    console.log("koikVormid: ", koikVormid)
    for (var i = 0; i < koikVormid.length; i++) {
        var vorm = koikVormid[i]
        vorm.onsubmit = async function(event) {
            event.preventDefault()
            console.log(event)
            var info_id = event.target[0].value
            console.log(info_id)
            var registreerija = event.target[1].value
            console.log(registreerija)

            await fetch(urlReg, {
                method: "POST",
                body: JSON.stringify({
                    info_id,
                    registreerija
                }),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            uuendaSisestust()
        }
    }
}
uuendaSisestust()