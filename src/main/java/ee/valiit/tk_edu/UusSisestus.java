package ee.valiit.tk_edu;

public class UusSisestus {              //Klass nimega UusSisestus, kuhu salvestatakse sisestatud andmed
    public String kuupaev;             //Loome muutujad
    public String algusaeg;
    public String lopuaeg;
    public String kool;
    public String aadress;
    public String linnmk;
    public String teema;
    public String klass;
    public String opilastearv;
    public String kontaktisik;
    public String kontaktemail;
    public String kontaktnumber;

    public UusSisestus(){} // 4. Tühi konstruktor, et Stringi postmapping töötaks

    public UusSisestus(String kuupaev, String algusaeg, String lopuaeg, String kool, String aadress, String linnmk, String teema, String klass, String opilastearv, String kontaktisik, String kontaktemail, String kontaktnumber){    //5. KONSTRUKTOR UusSisestus (tuleb APIkontrollerist, lisame siia muutujad
        this.kuupaev = kuupaev;
        this.algusaeg = algusaeg;
        this.lopuaeg = lopuaeg;
        this.kool = kool;
        this.aadress = aadress;
        this.linnmk = linnmk;
        this.teema = teema;
        this.klass = klass;
        this.opilastearv = opilastearv;
        this.kontaktisik = kontaktisik;
        this.kontaktemail = kontaktemail;
        this.kontaktnumber = kontaktnumber;
    }

    public String getKuupaev() {        //Teeme meetodi iga muutuja kohta (APIkontrolleris alt enteriga, et tagastaks sisestatud väärtuse
        return kuupaev;
    }

    public String getAlgusaeg() {
        return algusaeg;
    }

    public String getLopuaeg() {
        return lopuaeg;
    }

    public String getKool() {
        return kool;
    }

    public String getTeema() {
        return teema;
    }

    public String getAadress() { return aadress; }

    public String getLinnmk() {
        return linnmk;
    }

    public String getKlass() {
        return klass;
    }

    public String getOpilastearv() {
        return opilastearv;
    }

    public String getKontaktisik() {
        return kontaktisik;
    }

    public String getKontaktemail() {
        return kontaktemail;
    }

    public String getKontaktnumber() {
        return kontaktnumber;
    }
}
