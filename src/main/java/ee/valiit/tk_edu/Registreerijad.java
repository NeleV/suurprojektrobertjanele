package ee.valiit.tk_edu;

public class Registreerijad {
    public String info_id;
    public String registreerija;

    public Registreerijad() {}

    public Registreerijad(String id, String registreerija){
        this.info_id = id;
        this.registreerija = registreerija;
    }

    public String getRegistreerija() {
        return registreerija;
    }

    public String getInfo_id() {
        return info_id;
    }
}
