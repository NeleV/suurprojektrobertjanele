package ee.valiit.tk_edu;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

public class Email {

    public static void send(String emailTo, String subject, String msgHTML) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.mailtrap.io");
        props.put("mail.smtp.port", "465");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("32fcd273ffdc39", "636821e4b1b41b");  // mitte see millega sisse logid
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("info@tagasikooli.ee", false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
        msg.setSubject(subject);
        msg.setContent(msgHTML, "text/html");
        msg.setSentDate(new Date());

        Transport.send(msg);
        System.out.println("Email sent");
    }
}
