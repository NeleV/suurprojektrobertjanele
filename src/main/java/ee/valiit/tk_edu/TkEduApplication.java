package ee.valiit.tk_edu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.mail.MessagingException;
import java.io.IOException;

@SpringBootApplication
public class TkEduApplication implements CommandLineRunner {  		//Käivitab CommandLineRunner-i
	@Autowired
	JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {						//Main klass
		SpringApplication.run(TkEduApplication.class, args);
	}

	@Override
	public void run(String... args) throws IOException, MessagingException {
		String sqlKask = "CREATE TABLE IF NOT EXISTS info (id SERIAL, kuupaev TEXT, algusaeg TEXT, lopuaeg TEXT, kool TEXT, aadress TEXT, linnmk TEXT, teema TEXT, klass TEXT, opilastearv TEXT, kontaktisik TEXT, kontaktemail TEXT, kontaktnumber TEXT)";
		jdbcTemplate.execute(sqlKask);
		System.out.println("Info tabel loodud");
		//Email.send("info@tagasikooli.ee", "oled registreeritud!", "<p>html teade</p>");

		String sqlReg = "CREATE TABLE IF NOT EXISTS registreerijad (info_id INTEGER, registreerija TEXT)";
		jdbcTemplate.execute(sqlReg);
		System.out.println("Registreerijate tabel loodud");
	}
}

