package ee.valiit.tk_edu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;

import static org.springframework.jdbc.core.JdbcOperationsExtensionsKt.queryForObject;

@CrossOrigin
@RestController

public class APIController {
    @Autowired                      //Autowired: Käsk, mis ühendab ära jdbc klassi - ( ühenda Jdcb dependeci ühendus andmebaasiga)
            JdbcTemplate jdbcTemplate;
    private String registreerijadAndmebaasi;

    @PostMapping("/tk_edu")        //Postmapping APIkontrolleris, mis saadab päringu SQLi serverisse
    public void paringAndmebaasi(@RequestBody UusSisestus sisestus) {

        System.out.println("paringAndmebaasi");
        System.out.println("Kool: " + sisestus.getKool());

        String sqlKask = "INSERT INTO info (kuupaev, algusaeg, lopuaeg, kool, aadress, linnmk, teema, klass, opilastearv, kontaktisik, kontaktemail, kontaktnumber) VALUES ('"
                + sisestus.getKuupaev() + "', '"
                + sisestus.getAlgusaeg() + "', '"
                + sisestus.getLopuaeg() + "', '"
                + sisestus.getKool() + "', '"
                + sisestus.getAadress() + "', '"
                + sisestus.getLinnmk() + "', '"
                + sisestus.getTeema() + "', '"
                + sisestus.getKlass() + "', '"
                + sisestus.getOpilastearv() + "', '"
                + sisestus.getKontaktisik() + "', '"
                + sisestus.getKontaktemail() + "', '"
                + sisestus.getKontaktnumber() + "')";
        jdbcTemplate.execute(sqlKask);
        System.out.println("Sisestamine õnnestus!");
    }

    @GetMapping("/tk_edu")
    public ArrayList<UusSisestus> uusKooliSlot() {
        System.out.println("Uuskoolisloti käivitus");
        ArrayList<UusSisestus> loenguAeg = new ArrayList<>();
        loenguAeg = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM info LEFT JOIN registreerijad ON info.id=registreerijad.info_id WHERE info_id IS NULL ORDER BY kuupaev ASC, algusaeg ASC");
        System.out.println(loenguAeg);
        return loenguAeg;
    }

    @PostMapping("/tk_reg")
    public void registreerijadAndmebaasi(@RequestBody Registreerijad uusRegistreerija) throws IOException, MessagingException {
        System.out.println("registreerijadAndmebaasi");
        System.out.println("e-kiri: " + uusRegistreerija.getRegistreerija());

        String sqlReg = "INSERT INTO registreerijad (info_id, registreerija) VALUES ('"
                + uusRegistreerija.getInfo_id() + "', '" + uusRegistreerija.getRegistreerija() + "');";
        jdbcTemplate.execute(sqlReg);

        String emailRegistreerijale = uusRegistreerija.getRegistreerija();
        System.out.println(emailRegistreerijale);
        Email.send(emailRegistreerijale, "Oled loengule registreeritud!", "<p>Loengu teema ja muude üksikasjade täpsustamiseks võta ühendust kooliga!</p>");

        String id = uusRegistreerija.getInfo_id();
        System.out.println(id);
        String sqlEmailKoolile = (String) jdbcTemplate.queryForObject("SELECT kontaktemail FROM info WHERE id = " + id, String.class);
        System.out.println(sqlEmailKoolile);
        Email.send(sqlEmailKoolile, "Toimus loengule registreerimine!", "<p>Loengule registreerus külalisõpetaja!</p>");
    }
}
